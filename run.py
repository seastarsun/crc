import os
# _*_ coding: utf-8 _*_

def call_cmd(cmd = "ls"):
    print(cmd)
    subproc = os.popen(cmd)
    print(subproc.read())

def work():
    
    # os.makedirs(["./build"])
    # cmd_1 = "cmake . -G \"Unix Makefiles\" -B build" 
    # cmd_1 = "cmake . -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang -B build" 
    # cmd_1 = "cmake . -G \"Unix Makefiles\" -B build" 
    cmd_1 = "cmake . -G \"MinGW Makefiles\" -B build" 
    os.system(cmd_1)

    cmd_2 = "cmake --build ./build --config Release" 
    os.system(cmd_2)
    
    cmd_3 = ".\\out\\crc.exe" 
    call_cmd(cmd_3)


if __name__ == '__main__':
    work()