#ifndef _CRC_H_
#define _CRC_H_
#include "crc_func_def.h"

MAKE_CRC_FUNC_EXTERN(8)
MAKE_CRC_FUNC_EXTERN(16)
MAKE_CRC_FUNC_EXTERN(32)

#endif // !_CRC_H_
