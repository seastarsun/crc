#include "crc.h"



#define MAKE_CRC_S(bits)       \
    typedef struct             \
    {                          \
        char *name;            \
        uint##bits##_t Result; \
        uint##bits##_t Check;  \
        uint##bits##_t Poly;   \
        uint##bits##_t Init;   \
        uint##bits##_t RefIn;  \
        uint##bits##_t RefOut; \
        uint##bits##_t XorOut; \
    } crc##bits##_test_t;

MAKE_CRC_S(8)
MAKE_CRC_S(16)
MAKE_CRC_S(32)

crc8_test_t crc8_test_arr[] =
{
    // ���Ŷ���
    // Result, Check, Poly, Init, RefIn, RefOut, XorOut",
    {"CRC-8/AUTOSAR", 0xDF, 0xDF, 0x2F, 0xFF, 0, 0, 0xFF},
    {"CRC-8/BLUETOOTH", 0x26, 0x26, 0xA7, 0x00, 1, 1, 0x00},
    {"CRC-8/CDMA2000", 0xDA, 0xDA, 0x9B, 0xFF, 0, 0, 0x00},
    {"CRC-8/DARC", 0x15, 0x15, 0x39, 0x00, 1, 1, 0x00},
    {"CRC-8/DVB-S2", 0xBC, 0xBC, 0xD5, 0x00, 0, 0, 0x00},
    {"CRC-8/GSM-A", 0x37, 0x37, 0x1D, 0x00, 0, 0, 0x00},
    {"CRC-8/GSM-B", 0x94, 0x94, 0x49, 0x00, 0, 0, 0xFF},
    {"CRC-8/HITAG", 0xB4, 0xB4, 0x1D, 0xFF, 0, 0, 0x00},
    {"CRC-8/I-432-1", 0xA1, 0xA1, 0x07, 0x00, 0, 0, 0x55},
    {"CRC-8/I-CODE", 0x7E, 0x7E, 0x1D, 0xFD, 0, 0, 0x00},
    {"CRC-8/LTE", 0xEA, 0xEA, 0x9B, 0x00, 0, 0, 0x00},
    {"CRC-8/MAXIM-DOW", 0xA1, 0xA1, 0x31, 0x00, 1, 1, 0x00},
    {"CRC-8/MIFARE-MAD", 0x99, 0x99, 0x1D, 0xC7, 0, 0, 0x00},
    {"CRC-8/NRSC-5", 0xF7, 0xF7, 0x31, 0xFF, 0, 0, 0x00},
    {"CRC-8/OPENSAFETY", 0x3E, 0x3E, 0x2F, 0x00, 0, 0, 0x00},
    {"CRC-8/ROHC", 0xD0, 0xD0, 0x07, 0xFF, 1, 1, 0x00},
    {"CRC-8/SAE-J1850", 0x4B, 0x4B, 0x1D, 0xFF, 0, 0, 0xFF},
    {"CRC-8/SMBUS", 0xF4, 0xF4, 0x07, 0x00, 0, 0, 0x00},
    {"CRC-8/TECH-3250", 0x97, 0x97, 0x1D, 0xFF, 1, 1, 0x00},
    {"CRC-8/WCDMA", 0x25, 0x25, 0x9B, 0x00, 1, 1, 0x00},
};
crc16_test_t crc16_test_arr[] =
{
    //{ Name,Result, Check, Poly, Init, RefIn, RefOut, XorOut",
    {"CRC-16/ARC", 0xBB3D, 0xBB3D, 0x8005, 0x0000, 1, 1, 0x0000},
    {"CRC-16/CDMA2000", 0x4C06, 0x4C06, 0xC867, 0xFFFF, 0, 0, 0x0000},
    {"CRC-16/CMS", 0xAEE7, 0xAEE7, 0x8005, 0xFFFF, 0, 0, 0x0000},
    {"CRC-16/DDS-110", 0x9ECF, 0x9ECF, 0x8005, 0x800D, 0, 0, 0x0000},
    {"CRC-16/DECT-R", 0x007E, 0x007E, 0x0589, 0x0000, 0, 0, 0x0001},
    {"CRC-16/DECT-X", 0x007F, 0x007F, 0x0589, 0x0000, 0, 0, 0x0000},
    {"CRC-16/DNP", 0xEA82, 0xEA82, 0x3D65, 0x0000, 1, 1, 0xFFFF},
    {"CRC-16/EN-13757", 0xC2B7, 0xC2B7, 0x3D65, 0x0000, 0, 0, 0xFFFF},
    {"CRC-16/GENIBUS", 0xD64E, 0xD64E, 0x1021, 0xFFFF, 0, 0, 0xFFFF},
    {"CRC-16/GSM", 0xCE3C, 0xCE3C, 0x1021, 0x0000, 0, 0, 0xFFFF},
    {"CRC-16/IBM-3740", 0x29B1, 0x29B1, 0x1021, 0xFFFF, 0, 0, 0x0000},
    {"CRC-16/IBM-SDLC", 0x906E, 0x906E, 0x1021, 0xFFFF, 1, 1, 0xFFFF},
    {"CRC-16/ISO-IEC-14443-3-A", 0xBF05, 0xBF05, 0x1021, 0xC6C6, 1, 1, 0x0000},
    {"CRC-16/KERMIT", 0x2189, 0x2189, 0x1021, 0x0000, 1, 1, 0x0000},
    {"CRC-16/LJ1200", 0xBDF4, 0xBDF4, 0x6F63, 0x0000, 0, 0, 0x0000},
    {"CRC-16/M17", 0x772B, 0x772B, 0x5935, 0xFFFF, 0, 0, 0x0000},
    {"CRC-16/MAXIM-DOW", 0x44C2, 0x44C2, 0x8005, 0x0000, 1, 1, 0xFFFF},
    {"CRC-16/MCRF4XX", 0x6F91, 0x6F91, 0x1021, 0xFFFF, 1, 1, 0x0000},
    {"CRC-16/MODBUS", 0x4B37, 0x4B37, 0x8005, 0xFFFF, 1, 1, 0x0000},
    {"CRC-16/NRSC-5", 0xA066, 0xA066, 0x080B, 0xFFFF, 1, 1, 0x0000},
    {"CRC-16/OPENSAFETY-A", 0x5D38, 0x5D38, 0x5935, 0x0000, 0, 0, 0x0000},
    {"CRC-16/OPENSAFETY-B", 0x20FE, 0x20FE, 0x755B, 0x0000, 0, 0, 0x0000},
    {"CRC-16/PROFIBUS", 0xA819, 0xA819, 0x1DCF, 0xFFFF, 0, 0, 0xFFFF},
    {"CRC-16/RIELLO", 0x63D0, 0x63D0, 0x1021, 0xB2AA, 1, 1, 0x0000},
    {"CRC-16/SPI-FUJITSU", 0xE5CC, 0xE5CC, 0x1021, 0x1D0F, 0, 0, 0x0000},
    {"CRC-16/T10-DIF", 0xD0DB, 0xD0DB, 0x8BB7, 0x0000, 0, 0, 0x0000},
    {"CRC-16/TELEDISK", 0x0FB3, 0x0FB3, 0xA097, 0x0000, 0, 0, 0x0000},
    {"CRC-16/TMS37157", 0x26B1, 0x26B1, 0x1021, 0x89EC, 1, 1, 0x0000},
    {"CRC-16/UMTS", 0xFEE8, 0xFEE8, 0x8005, 0x0000, 0, 0, 0x0000},
    {"CRC-16/USB", 0xB4C8, 0xB4C8, 0x8005, 0xFFFF, 1, 1, 0xFFFF},
    {"CRC-16/XMODEM", 0x31C3, 0x31C3, 0x1021, 0x0000, 0, 0, 0x0000},
};

crc32_test_t crc32_test_arr[] =
{
    //{ Name,Result, Check, Poly, Init, RefIn, RefOut, XorOut",
    {"CRC-32/AIXM", 0x3010BF7F, 0x3010BF7F, 0x814141AB, 0x00000000, 0, 0, 0x00000000},
    {"CRC-32/AUTOSAR", 0x1697D06A, 0x1697D06A, 0xF4ACFB13, 0xFFFFFFFF, 1, 1, 0xFFFFFFFF},
    {"CRC-32/BASE91-D", 0x87315576, 0x87315576, 0xA833982B, 0xFFFFFFFF, 1, 1, 0xFFFFFFFF},
    {"CRC-32/BZIP2", 0xFC891918, 0xFC891918, 0x04C11DB7, 0xFFFFFFFF, 0, 0, 0xFFFFFFFF},
    {"CRC-32/CD-ROM-EDC", 0x6EC2EDC4, 0x6EC2EDC4, 0x8001801B, 0x00000000, 1, 1, 0x00000000},
    {"CRC-32/CKSUM", 0x765E7680, 0x765E7680, 0x04C11DB7, 0x00000000, 0, 0, 0xFFFFFFFF},
    {"CRC-32/ISCSI", 0xE3069283, 0xE3069283, 0x1EDC6F41, 0xFFFFFFFF, 1, 1, 0xFFFFFFFF},
    {"CRC-32/ISO-HDLC", 0xCBF43926, 0xCBF43926, 0x04C11DB7, 0xFFFFFFFF, 1, 1, 0xFFFFFFFF},
    {"CRC-32/JAMCRC", 0x340BC6D9, 0x340BC6D9, 0x04C11DB7, 0xFFFFFFFF, 1, 1, 0x00000000},
    {"CRC-32/MEF", 0xD2C22F51, 0xD2C22F51, 0x741B8CD7, 0xFFFFFFFF, 1, 1, 0x00000000},
    {"CRC-32/MPEG-2", 0x0376E6E7, 0x0376E6E7, 0x04C11DB7, 0xFFFFFFFF, 0, 0, 0x00000000},
    {"CRC-32/XFER", 0xBD0BE338, 0xBD0BE338, 0x000000AF, 0x00000000, 0, 0, 0x00000000},
};


#define MAKE_CRC_TEST_FUNC(bits)                                                                                          \
    void crc##bits##_test()                                                                                               \
    {                                                                                                                     \
        char *data = "123456789";                                                                                         \
        uint32_t i;                                                                                                       \
        uint32_t len = sizeof(crc##bits##_test_arr) / sizeof(crc##bits##_test_t);                                         \
        printf("\nCRC-%d:Name,             Result,  Check,  Poly, Init, RefIn, RefOut, XorOut, CompareResult \n", bits);  \
        for (i = 0; i < len; i++)                                                                                         \
        {                                                                                                                 \
            crc##bits##_test_t *pcrc = &crc##bits##_test_arr[i];                                                          \
            if (pcrc->RefIn)                                                                                              \
            {                                                                                                             \
                pcrc->Result = crc##bits##_r((uint8_t *)data, 9, pcrc->Poly, pcrc->Init, pcrc->XorOut);                   \
            }                                                                                                             \
            else                                                                                                          \
            {                                                                                                             \
                pcrc->Result = crc##bits##_n((uint8_t *)data, 9, pcrc->Poly, pcrc->Init, pcrc->XorOut);                   \
            }                                                                                                             \
                                                                                                                          \
            if (bits == 8)                                                                                                \
            {                                                                                                             \
                printf("%-25s: 0x%02x, 0x%02x, 0x%02x, 0x%02x,%d,%d, 0x%02x,%d\n", pcrc->name, pcrc->Result, pcrc->Check, \
                       pcrc->Poly, pcrc->Init, pcrc->RefIn, pcrc->RefOut, pcrc->XorOut, pcrc->Result == pcrc->Check);     \
            }                                                                                                             \
            else if (bits == 16)                                                                                          \
            {                                                                                                             \
                printf("%-25s: 0x%04x, 0x%04x, 0x%04x, 0x%04x,%d,%d, 0x%04x,%d\n", pcrc->name, pcrc->Result, pcrc->Check, \
                       pcrc->Poly, pcrc->Init, pcrc->RefIn, pcrc->RefOut, pcrc->XorOut, pcrc->Result == pcrc->Check);     \
            }                                                                                                             \
            else                                                                                                          \
            {                                                                                                             \
                printf("%-25s: 0x%08x, 0x%08x, 0x%08x, 0x%08x,%d,%d, 0x%08x,%d\n", pcrc->name, pcrc->Result, pcrc->Check, \
                       pcrc->Poly, pcrc->Init, pcrc->RefIn, pcrc->RefOut, pcrc->XorOut, pcrc->Result == pcrc->Check);     \
            }                                                                                                             \
        }                                                                                                                 \
    }

MAKE_CRC_TEST_FUNC(8)
MAKE_CRC_TEST_FUNC(16)
MAKE_CRC_TEST_FUNC(32)

void crc_test()
{
    crc8_test();
    crc16_test();
    crc32_test();
}

